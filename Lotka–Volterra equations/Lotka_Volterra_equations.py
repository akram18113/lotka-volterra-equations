import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

# Define the Lotka-Volterra equations
def lotka_volterra(t, y, a, b, c, d):
    x, y = y
    dxdt = a*x - b*x*y
    dydt = -c*y + d*x*y
    return [dxdt, dydt]

# Define the initial conditions and parameters
x0 = 10
y0 = 2
a = 1
b = 0.1
c = 1.5
d = 0.075

# Set the time span and solve the differential equations
t_span = [0, 30]
sol = solve_ivp(lotka_volterra, t_span, [x0, y0], args=(a, b, c, d))

# Plot the results
plt.plot(sol.t, sol.y[0], label='Prey')
plt.plot(sol.t, sol.y[1], label='Predator')
plt.xlabel('Time')
plt.ylabel('Population')
plt.title('Lotka-Volterra Predator-Prey Model')
plt.legend()
plt.show()
